package com.fragment.kevinalejandrocastano.fragment.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.fragment.kevinalejandrocastano.fragment.Fragments.DataFragment;
import com.fragment.kevinalejandrocastano.fragment.Fragments.DetailsFragment;
import com.fragment.kevinalejandrocastano.fragment.R;

public class MainActivity extends FragmentActivity implements DataFragment.DataListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void sendData(String text) {
        //llamar al metodo renderizar texto de el DetailsFrament,
        //pasando el texto que recibimos por parametro en este mismo metodo

        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
        detailsFragment.renderText(text);
    }
}
